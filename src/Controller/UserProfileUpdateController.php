<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Form\ProfileUpdateType;


class UserProfileUpdateController extends Controller
{
    /**
     * @Route("/user/profile/update/user", name="user_profile_update")
     * @Security("has_role('USER')")
     */
    public function index(Request $request, UserRepository $repo)
    {
        
        $user = $this->getUser();
        $form = $this->createForm(ProfileUpdateType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                // perform some action...
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('home');
            

        }
        return $this->render('user_profile_update/index.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);

    }
}
