<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Service\AddProductService;

class GestionController extends Controller
{
    /**
     * @Route("/gestion/admin", name="gestion")
     */
    public function index(ProductRepository $repository)
    {
        
        $products = $repository->findAll();

        return $this->render('gestion/index.html.twig', [
            'controller_name' => 'GestionController',
            'products' => $products
        ]);
    }
}
