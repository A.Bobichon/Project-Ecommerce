<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;
use App\Service\SelectShoppingCartService;


class FullPageProductController extends Controller
{
    /**
     * @Route("/full/page/product", name="full_page_product")
     */
    public function index(SelectShoppingCartService $services)
    {
        $em = $this->getDoctrine()->getRepository(Product::class);
        $products = $em->findAll();

        /// test
        $user = $services->getCurrentUser();
        dump($user);

        ///end test
        return $this->render("full_page_product/index.html.twig", [
             'products' => $products,
             'user' => $user
             ]);
    }

    /**
     * @Route("/full/page/addProduct/{id}", name="add_product_id")
     */
    public function addProduct(int $id, SelectShoppingCartService $services)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);
        $user = $services->getCurrentUser();
        $services->appendToLineProduct($product);
        return $this->redirectToRoute('full_page_product');
    }
}
